import React from 'react'
import appLogo from '../assets/app_logo.png';

const Header = ({title}) => {
    return (
        <div style = {{height:'7em'}}>
            <img style = {{float:'left'}} src={appLogo} className = "app-logo" alt = "logo" />
            <h1 style = {{textAlign:'center'}} className = "app-title">{title}</h1>
        </div>
    )
}

Header.defaultProps = {
    title: "User Management"
}

export default Header
