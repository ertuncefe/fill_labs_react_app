import React from 'react'
import ListItem from './ListItem'

const UserList = () => {
    return (
        <div style = {{padding:'2em'}}>
            <ListItem/>
            <ListItem/>
            <ListItem/>
            <ListItem/>
            <ListItem/>
            <ListItem/>
            <ListItem/>
        </div>
    )
}

export default UserList
