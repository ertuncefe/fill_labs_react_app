import React from 'react'
import { useState } from 'react' 



const ListItem = (props) => {

    const [users, setUsers] = useState([
        {
            id:1,
            profilePicture:'/d0S+uQUub7STIa',
            name: "Hasan",
            surname: "Güneş",
            profession: "Project Lead",
            birthdate: "03-04-1995",
            gender: "male"
        }
    ]);

    return (
        <div style = {{marginBottom:'1em', padding:'1em', height:'5em', display:'flex', flexDirection:'row', justifyContent:'space-between', backgroundColor:'#F5F5F5', border: 'solid black 1px', borderRadius:'7px', flexDirection:'row'}}>
            <img src = {{uri: `data:image/png;base64,${users[0].profilePicture}`}} />
            <h3>{users[0].name + " " + users[0].surname}</h3>
            <button style = {{padding:'1em'}}>edit</button>
            <button style = {{padding:'1em'}}>delete</button>
        </div>
    )
}

export default ListItem
