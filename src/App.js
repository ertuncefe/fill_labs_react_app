import './App.css';
import Header from './components/Header'
import UserList from './components/UserList';

function App() {
  return (
    <div className = "container">
      <Header title= 'Fill Labs User Management'/>
      <div className = 'top' style={{width:'100%', flexDirection:'row'}}>
        <button style = {{width:'3em', height:'3em', marginLeft:'85%'}}>+</button>
      </div>
      <UserList/>
    </div>
  );
}

export default App;
